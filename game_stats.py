
high_score_file = "high_score.txt"
#open(high_score_file, 'w') as file_object
#file_object.write("new high score, yay")

class GameStats:
    """Track statistics for Alien Invasion."""
    
    def __init__(self, ai_game):
        """Initialize statistics."""
        self.settings = ai_game.settings
        self.reset_stats()
        
        # start game in an inactive state
        self.game_active = False
        
        # high score is read from a local file
        with open(high_score_file,'r') as file_object:
            line = file_object.readline()
            try:
                high_score = int(line)
            except ValueError:
                high_score = 0

        #print(high_score)
        self.high_score = high_score
        
        
    def reset_stats(self):
        """Init stats that can change during the game."""
        self.ships_left = self.settings.ship_limit
        self.score = 0
        self.level = 1
        
        
    def save_high_score(self):
        """Save the high score to file."""
        with open(high_score_file,'w') as file_object:
            file_object.write(str(self.high_score))
       
        